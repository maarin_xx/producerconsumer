// класс потребителя
public class Consumer implements Runnable{
    Queue queue;
    String name;

    Consumer(Queue queue, String name){
        this.queue = queue;
        this.name = name;
        new Thread(this, " Consumer").start();
    }

    @Override
    public void run() {
        while (true){
            try {
                queue.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name);
        }
    }
}
