package bank;

public class PC {
    public static void main(String args[]) throws InterruptedException {
        Queue queue = new Queue();
        //new Producer(queue);
        Producer p1 = new Producer(queue, "Producer1");
        Producer p2 = new Producer(queue, "Producer2");
        Producer p3 = new Producer(queue, "Producer3");
        Consumer c1 = new Consumer(queue, "Consumer1");
        Consumer c2 = new Consumer(queue, "Consumer2");
        Consumer c3 = new Consumer(queue, "Consumer3");
        Consumer c4 = new Consumer(queue, "Consumer4");
    }
}