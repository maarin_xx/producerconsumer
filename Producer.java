
//класс поставщика
public class Producer implements Runnable {
    Queue queue;
    String name;

    Producer(Queue queue, String name) {
        this.queue = queue;
        this.name = name;
        new Thread(this, "Producer").start();
    }

    public void run() {
        int i = 0;
        while (true) {
            queue.put(i++);
            System.out.print(name);
        }
    }
}