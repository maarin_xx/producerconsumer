// класс очереди
public class Queue {

    int n, m = 0;
    boolean isSet = false;


    synchronized int get() throws InterruptedException {

       if (!isSet) {
           // если добавили товар m=1
          // if (m==1){
          //     try {

            //       wait();
           //        Thread.sleep(1000);
           //    } catch (InterruptedException e) {
           //        System.out.println("Thread is interrupted. Exception!");
           //    }


                try {

                    wait();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("Thread is interrupted. Exception!");
                }
            }
      // }


        System.out.print(" Got: " + n + " ");
       //m--;


        isSet = false;
               notify();

        return n;


    }

    synchronized void put(int n) {
        if (isSet) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    System.out.println("Thread is interrupted. Exception!");
                }
            }
        this.n = n;
        isSet = true;
        System.out.println("Put: " + n + " ");
       // m++;
        notifyAll();
    }
}